/* AXI DMA Example
*
* This small example is intended to simply llustate how to use the DMA engine
* of Linux to take advantage of DMA in the PL. The hardware design is intended
* to be an AXI DMA without scatter gather and with the transmit channel looped
* back to the receive channel.
*
*/


/* This code is based on example code from Xilinx and then modified to use
 * the device tree from axidmatest that is included in the kernel source.
 * The in-tree axidmatest is more complicated than it needs to be for example
 * code and also requires the scatter gather to be used.
 * https://forums.xilinx.com/xlnx/attachments/xlnx/ELINUX/10658/1/drivers-session4-dma-4public.pdf
 * https://forums.xilinx.com/xlnx/attachments/xlnx/ELINUX/10658/2/axidma.c.golden
 */


#include <linux/dmaengine.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/dma-mapping.h>
#include <linux/slab.h>
#include <linux/of_dma.h>
#include <linux/platform_device.h>

static struct dma_chan *tx_chan;
static struct dma_chan *rx_chan;
static struct completion tx_cmp;
static struct completion rx_cmp;
static dma_cookie_t tx_cookie;
static dma_cookie_t rx_cookie;
static dma_addr_t tx_dma_handle;
static dma_addr_t rx_dma_handle;

#define WAIT 	1
#define NO_WAIT 0

/* Handle a callback and indicate the DMA transfer is complete to another
 * thread of control
 */
static void axidma_sync_callback(void *completion)
{
	/* Step 9, indicate the DMA transaction completed to allow the other
	 * thread of control to finish processing
	 */

	complete(completion);

}

/* Prepare a DMA buffer to be used in a DMA transaction, submit it to the DMA engine
 * to queued and return a cookie that can be used to track that status of the
 * transaction
 */
static dma_cookie_t axidma_prep_buffer(struct dma_chan *chan, dma_addr_t buf, size_t len,
					enum dma_transfer_direction dir, struct completion *cmp)
{
	enum dma_ctrl_flags flags = DMA_CTRL_ACK | DMA_PREP_INTERRUPT;
	struct dma_async_tx_descriptor *chan_desc;
	dma_cookie_t cookie;

	/* Step 5, create a buffer (channel)  descriptor for the buffer since only a
	 * single buffer is being used for this transfer
	 */

	chan_desc = dmaengine_prep_slave_single(chan, buf, len, dir, flags);

	/* Make sure the operation was completed successfully
	 */
	if (!chan_desc) {
		printk(KERN_ERR "dmaengine_prep_slave_single error\n");
		cookie = -EBUSY;
	} else {
		chan_desc->callback = axidma_sync_callback;
		chan_desc->callback_param = cmp;

		/* Step 6, submit the transaction to the DMA engine so that it's queued
		 * up to be processed later and get a cookie to track it's status
		 */

		cookie = dmaengine_submit(chan_desc);

	}
	return cookie;
}

/* Start a DMA transfer that was previously submitted to the DMA engine and then
 * wait for it complete, timeout or have an error
 */
static void axidma_start_transfer(struct dma_chan *chan, struct completion *cmp,
					dma_cookie_t cookie, int wait)
{
	unsigned long timeout = msecs_to_jiffies(3000);
	enum dma_status status;

	/* Step 7, initialize the completion before using it and then start the
	 * DMA transaction which was previously queued up in the DMA engine
	 */

	init_completion(cmp);
	dma_async_issue_pending(chan);

	if (wait) {
		printk("Waiting for DMA to complete...\n");

		/* Step 8, wait for the transaction to complete, timeout, or get
		 * get an error
		 */

		timeout = wait_for_completion_timeout(cmp, timeout);
		status = dma_async_is_tx_complete(chan, cookie, NULL, NULL);

		/* Determine if the transaction completed without a timeout and
		 * withtout any errors
		 */
		if (timeout == 0)  {
			printk(KERN_ERR "DMA timed out\n");
		} else if (status != DMA_COMPLETE) {
			printk(KERN_ERR "DMA returned completion callback status of: %s\n",
			       status == DMA_ERROR ? "error" : "in progress");
		}
	}
}

static void axidma_test_transfer(void)
{
	const int dma_length = 32*1024;
	int i;

	/* Step 3, allocate cached memory for the transmit and receive buffers to use for DMA
	 * zeroing the destination buffer
	 */

	char *src_dma_buffer = kmalloc(dma_length, GFP_KERNEL);
	char *dest_dma_buffer = kzalloc(dma_length, GFP_KERNEL);

	if (!src_dma_buffer || !dest_dma_buffer) {
		printk(KERN_ERR "Allocating DMA memory failed\n");
		return;
	}

	/* Initialize the source buffer with known data to allow the destination buffer to
	 * be checked for success
	 */
	for (i = 0; i < dma_length; i++)
		src_dma_buffer[i] = i;

	/* Step 4, since the CPU is done with the buffers, transfer ownership to the DMA and don't
	 * touch the buffers til the DMA is done, transferring ownership may involve cache operations
	 */

	tx_dma_handle = dma_map_single(tx_chan->device->dev, src_dma_buffer, dma_length, DMA_TO_DEVICE);
	rx_dma_handle = dma_map_single(rx_chan->device->dev, dest_dma_buffer, dma_length, DMA_FROM_DEVICE);

	/* Prepare the DMA buffers and the DMA transactions to be performed and make sure there was not
	 * any errors
	 */
	rx_cookie = axidma_prep_buffer(rx_chan, rx_dma_handle, dma_length, DMA_DEV_TO_MEM, &rx_cmp);
	tx_cookie = axidma_prep_buffer(tx_chan, tx_dma_handle, dma_length, DMA_MEM_TO_DEV, &tx_cmp);

	if (dma_submit_error(rx_cookie) || dma_submit_error(tx_cookie)) {
		printk(KERN_ERR "xdma_prep_buffer error\n");
		return;
	}

	printk(KERN_INFO "Starting DMA transfers\n");

	/* Start both DMA transfers and wait for them to complete
	 */
	axidma_start_transfer(rx_chan, &rx_cmp, rx_cookie, NO_WAIT);
	axidma_start_transfer(tx_chan, &tx_cmp, tx_cookie, WAIT);

	/* Step 10, the DMA is done with the buffers so transfer ownership back to the CPU so that
	 * any cache operations needed are done
	 */

	dma_unmap_single(rx_chan->device->dev, rx_dma_handle, dma_length, DMA_FROM_DEVICE);
	dma_unmap_single(tx_chan->device->dev, tx_dma_handle, dma_length, DMA_TO_DEVICE);

	/* Verify the data in the destination buffer matches the source buffer
	 */
	for (i = 0; i < dma_length; i++) {
		if (dest_dma_buffer[i] != src_dma_buffer[i]) {
			printk(KERN_INFO "DMA transfer failure");
			print_hex_dump(KERN_ALERT, "mem_src: ", DUMP_PREFIX_ADDRESS,
                16, 1, src_dma_buffer, dma_length, 1);
			print_hex_dump(KERN_ALERT, "mem_dst: ", DUMP_PREFIX_ADDRESS,
                16, 1, dest_dma_buffer, dma_length, 1);
			break;
		}
	}

	printk(KERN_INFO "DMA bytes sent: %d\n", dma_length);

	/* Step 11, free the buffers used for DMA back to the kernel
	 */

	kfree(src_dma_buffer);
	kfree(dest_dma_buffer);

}

static int axidmaloopback_probe(struct platform_device *pdev)
{
	dma_cap_mask_t mask;

	printk(KERN_INFO "AXI DMA module initialized\n");

	/* Step 1, zero out the capability mask then initialize
	 * it for a slave channel that is private
	 */

	dma_cap_zero(mask);
	dma_cap_set(DMA_SLAVE | DMA_PRIVATE, mask);

	/* Step 2, request the transmit and receive channels for the AXI DMA
	 * from the DMA engine
	 */

	tx_chan = dma_request_slave_channel(&pdev->dev, "axidma0");
	rx_chan = dma_request_slave_channel(&pdev->dev, "axidma1");

	if (!rx_chan || !tx_chan) {
		printk(KERN_INFO "DMA channel request error\n");
		return -1;
	}

	axidma_test_transfer();

	/* Step 12, release the DMA channels back to the DMA engine
	 */

	dma_release_channel(tx_chan);
	dma_release_channel(rx_chan);

	return 0;
}

static int axidmaloopback_remove(struct platform_device *pdev)
{
	printk(KERN_INFO "AXI DMA module exited\n");
	return 0;
}


static const struct of_device_id xilinx_axidmatest_of_ids[] = {
	{ .compatible = "xlnx,axi-dma-test-1.00.a",},
	{}
};

static struct platform_driver axidmaloopback_driver = {
	.driver = {
		.name = "axidmaloopback",
		.owner = THIS_MODULE,
		.of_match_table = xilinx_axidmatest_of_ids,
	},
	.probe = axidmaloopback_probe,
	.remove = axidmaloopback_remove,
};

static int __init axidmaloopback_init(void)
{
	return platform_driver_register(&axidmaloopback_driver);
}

static void __exit axidmaloopback_exit(void)
{
	platform_driver_unregister(&axidmaloopback_driver);
}

late_initcall(axidmaloopback_init);
module_exit(axidmaloopback_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Simple Xilinx AXIDMA loopback test");
MODULE_VERSION("0.01");
